export interface AttributeInterface {
  name: string
  description: string
  typePersonalInformation: string
}

export interface TableInterface {
  name: string
  description: string
  selectionCriteria: string
  attributes: Array<AttributeInterface>
  isTransfer: boolean
  isTruncate: boolean
  id: number
}

export interface StoreInterface {
  name: string
  description: string
  jdbcUrl: string
  dbType: 'ORACLE'
  tables: Array<TableInterface>
  id: number
}

export interface ModelInterface {
  id: number
  name: string
  description: string
  stores: Array<StoreInterface>
}

export interface ModelState {
  model: ModelInterface | undefined
  loading: boolean
  persData: boolean
  showenStorages: Array<StoreInterface> | undefined
  error: boolean
}