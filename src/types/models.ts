export interface ModelsInterface {
  id: number
  name: string
  description: string
}

export interface ModelsState {
  models: Array<ModelsInterface> | []
  loading: boolean
  error: boolean
}