import { configureStore } from '@reduxjs/toolkit'
import modelReducer from './slices/model'
import modelsReducer from './slices/models'

export const store = configureStore({
  reducer: {
    model: modelReducer,
    models: modelsReducer
  }
})

export default store
export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch