import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import { ModelsState } from '../../types/models'

const API = 'http://localhost:8080/models'

export const fetchModels = createAsyncThunk(
  'models/fetchModels',
  async (_, {rejectWithValue}) => {
    try {
      const response = await fetch(API)
      if(!response.ok) {
        throw new Error('Error')
      }
      const data = await response.json()
      return data
    } catch(error) {
      return rejectWithValue(error)
    }
    
  }
)

const initialState: ModelsState = {
  models: [],
  loading: false,
  error: false
}

export const modelsSlice = createSlice({
  name: 'models',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchModels.pending, (state) => {
      state.loading = true
    })
    builder.addCase(fetchModels.fulfilled, (state, action) => {
      state.models = action.payload
      state.loading = false
    })
    builder.addCase(fetchModels.rejected, (state, action) => {
      state.loading = false
      state.error = true
      console.log(action.payload)
    })
  }
})

export default modelsSlice.reducer