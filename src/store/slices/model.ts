import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import { ModelState, ModelInterface, StoreInterface } from '../../types/model'

export const fetchModel = createAsyncThunk<
  ModelInterface,
  { modelId: string } & Partial<ModelInterface>
>(
  'model/fetchModel',
  async ({modelId}, {rejectWithValue}) => {
    try {
      const response = await fetch(`http://localhost:8080/models/${modelId}`)
      if(!response.ok) {
        throw new Error('Error')
      }
      const data = await response.json()
      return data
    } catch(error) {
      return rejectWithValue(error)
    }
    
  }
)

const initialState: ModelState = {
  model: undefined,
  loading: false,
  persData: false,
  showenStorages: undefined,
  error: false
}

export const modelSlice = createSlice({
  name: 'model',
  initialState,
  reducers: {
    toggleFilter: (state) => {
      if(state.persData) {
        state.showenStorages = state.model?.stores
        state.persData = !state.persData
      } else {
        const newStorages = state.showenStorages
        newStorages?.forEach(item =>
          item.tables = item.tables.filter(itm => {
            return itm.attributes.some(i => {
              return i.typePersonalInformation !== 'none'
            })
          })
        )
        state.showenStorages = newStorages?.filter(item => {
          return !!item.tables.length
        })
        state.persData = !state.persData
      }
    }
  },
  extraReducers: (builder) => {
    builder.addCase(fetchModel.pending, (state) => {
      state.loading = true
    })
    builder.addCase(fetchModel.fulfilled, (state, action) => {
      state.model = action.payload
      state.showenStorages = ArrayToObject(action.payload.stores)
      state.loading = false
      state.persData = false
    })
    builder.addCase(fetchModel.rejected, (state, action) => {
      state.loading = false
      state.error = true
      console.log(action.payload)
    })
  }
})

export const { toggleFilter } = modelSlice.actions

export default modelSlice.reducer

function ArrayToObject(arr: Array<StoreInterface>) {
  for(let i = 0; i < arr.length; i++) {
    arr[i].id = i
    for(let j = 0; j < arr[i].tables.length; j++) {
      arr[i].tables[j].id = j
    }
  }
  return arr
}