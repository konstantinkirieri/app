import NewStore from '../NewStore'
import { useState } from 'react'
import TextField from '@material-ui/core/TextField'
import { makeStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'

const useStyle = makeStyles(() => ({
  container: {
    margin: 'auto',
    marginTop: 50,
    width: 400,
    display: 'flex',
    flexDirection: 'column'
  },
  button: {
    marginTop: 20,
    width: 120
  }
}))



const NewModel: React.FC = () => {
  const classes = useStyle()

  const [countStore, setCountStore] = useState(0);
  const [modelName, setModelName] = useState('');
  const [modelDescription, setModelDescription] = useState('');

  const handleChangeModelName = (event: React.ChangeEvent<HTMLInputElement>) => {
    setModelName(event.target.value);
  };
  const handleChangeModelDescription = (event: React.ChangeEvent<HTMLInputElement>) => {
    setModelDescription(event.target.value);
  };
  const handleAddStore = () => {
    setCountStore(() => countStore + 1)
  }
  const createStoresForm = () => {
    const stores = []
    for(let i = 1; i <= countStore; i++) {
      stores.push(<NewStore key={i}/>)
    }
    return stores.map(item => item)
  }
  const handleSubmitStore = (event: any) => {
    event?.preventDefault()
    console.log(event.target.modelName)
  }
  

  return(
    <form className={classes.container} onSubmit={handleSubmitStore}>
      <TextField
        name='modelName'
        id="standard-multiline-flexible"
        label="Name model"
        multiline
        maxRows={1}
      />
      <TextField
        id="standard-multiline-flexible"
        label="Description model"
        multiline
        maxRows={4}
      />
      {createStoresForm()}
      <Button variant="contained" color="primary" className={classes.button} onClick={handleAddStore}>
        Add store
      </Button>
      <Button type='submit' variant="contained" color="primary">
        Submit
      </Button>
    </form>
  )
}

export default NewModel