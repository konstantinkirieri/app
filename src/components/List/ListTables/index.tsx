import React from 'react'
import { Link, useParams } from 'react-router-dom'
import { TableInterface } from '../../../types/model'
import { useAppSelector } from '../../../hooks'
import Button from '@material-ui/core/Button'
import { makeStyles } from '@material-ui/core/styles'

const useStyle = makeStyles(() => ({
  list: {
    width: 200,
    backgroundColor: '#6C8CD5'
  },
  listItem: {
    width: 200,
    textAlign: 'center',
    color: 'white'
  }
}))

const List: React.FC = () => {
  const { showenStorages } = useAppSelector(state => state.model)
  const { id, modelId } = useParams<{id: string, modelId: string}>()
  const classes = useStyle()
  let items
  if(showenStorages) items = showenStorages.find(item => item.id == +id)?.tables
  
  return (
    <div className={classes.list}>
      {items ? items.map((item: TableInterface) =>
        <Link to={`/models/${modelId}/stores/${id}/table/${item.id}`} key={item.name}>
          <Button variant="contained" color="primary" className={classes.listItem}>
            {item.name}
          </Button>
        </Link>) : 'Don\'t have table'}
    </div>
  )
}

export default List