import React from 'react';
import { Link, useParams } from 'react-router-dom'
import { useAppSelector } from '../../../hooks'
import { StoreInterface } from '../../../types/model'
import { makeStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'

const useStyle = makeStyles(() => ({
  list: {
    width: 200,
    backgroundColor: '#6C8CD5'
  },
  listItem: {
    width: 200,
    textAlign: 'center',
    color: 'white'
  }
}))

const List: React.FC = () => {
  const { modelId } = useParams<{modelId: string}>()
  const classes = useStyle()
  const { showenStorages: items, loading, error } = useAppSelector((state) => state.model);

  if(loading) return <div>LOADING</div>
  if(error) return <div>ERROR</div>
  return (
    <div className={classes.list}>
      {items?.length ? items.map((item: StoreInterface) => (
        <Link to={`/models/${modelId}/stores/${item.id}`} key={item.name}>
          <Button variant="contained" color="primary" className={classes.listItem}>
            {item.name}
          </Button>
        </Link>
      )) : 'Don\'t have stores'}
    </div>
  );
};

export default List;