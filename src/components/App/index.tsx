import Header from '../Header'
import ListStores from '../List/ListStores'
import ListTables from '../List/ListTables'
import DescriptionStore from '../DescriptionStore'
import DescriptionTable from '../DescriptionTable'
import Models from '../Models'
import Model from '../Model'
import NewModel from '../NewModel'
import Footer from '../Footer'
import S from './styles.module.css'
import React from 'react'
import { BrowserRouter, Switch, Redirect, Route } from 'react-router-dom';

const App: React.FC = () => {
  return (
    <BrowserRouter>
      <Header />
      <main className={S.main}>
        <Switch>
          <Route path="/models/:modelId/stores/:id/table/:tableId">
            <Model>
              <ListTables/>
              <DescriptionStore>
                <DescriptionTable />
              </DescriptionStore>
            </Model>
          </Route>
          <Route path="/models/:modelId/stores/:id">
            <Model>
              <ListTables />
              <DescriptionStore />
            </Model>
          </Route>
          <Route path="/models/:modelId">
            <Model> 
              <ListStores />
            </Model>
          </Route>
          <Route path="/models">
            <Models />
          </Route>
          <Route path="/new_model">
            <NewModel/>
          </Route>
          <Route path="/">
            <Redirect to="/models" />
          </Route>
        </Switch>
      </main>
      <Footer />
    </BrowserRouter>
  );
};
export default App;
