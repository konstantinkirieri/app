import { useParams } from 'react-router-dom'
import { useAppSelector } from '../../hooks'
import S from './styles.module.css'

const DescriptionStore: React.FC = ({children}) => {
  const { id } = useParams<{id: string}>()
  const stores = useAppSelector(state => state.model.model?.stores)
  let store
  if(stores) store = stores.find(item => item.id === +id)
  

  if(!store) return <div>Пусто</div>
  return (
    <div className={S.container}>
      <div className={S.storeDescription}>
        <h3>Store</h3>
        <table>
          <tbody>
            <tr>
              <td>Name</td>
              <td>{store?.name}</td>
            </tr>
            <tr>
              <td>Description</td>
              <td>{store?.description}</td>
            </tr>
          </tbody>
        </table>
      </div>
      {children}
    </div>
  )
}

export default DescriptionStore