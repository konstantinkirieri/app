import TextField from '@material-ui/core/TextField'
import { makeStyles } from '@material-ui/core/styles'
import { useState } from 'react'

const useStyle = makeStyles(() => ({
  container: {
    margin: 'auto',
    marginTop: 50,
    width: 400,
    display: 'flex',
    flexDirection: 'column'
  },
  button: {
    marginTop: 20,
    width: 120
  }
}))

const NewStore: React.FC = () => {
  const [storeName, setStoreName] = useState('');
  const [storeDescription, setStoreDescription] = useState('');

  const classes = useStyle()

  const handleChangeStoreName = (event: React.ChangeEvent<HTMLInputElement>) => {
    setStoreName(event.target.value);
  };
  const handleChangeStoreDescription = (event: React.ChangeEvent<HTMLInputElement>) => {
    setStoreDescription(event.target.value);
  };

  return (
    <div className={classes.container}>
      <TextField
        id="standard-multiline-flexible"
        label="Name store"
        multiline
        maxRows={1}
        value={storeName}
        onChange={handleChangeStoreName}
      />
      <TextField
        id="standard-multiline-flexible"
        label="Description store"
        multiline
        maxRows={4}
        value={storeDescription}
        onChange={handleChangeStoreDescription}
      />
    </div>
  )
}
export default NewStore