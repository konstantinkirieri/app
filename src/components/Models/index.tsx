import {useEffect} from 'react'
import { fetchModels } from '../../store/slices/models'
import { useAppDispatch, useAppSelector } from '../../hooks'
import { Link } from 'react-router-dom'
import { makeStyles } from '@material-ui/core/styles'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
  container: {
    maxWidth: 1000,
    margin: 'auto',
    marginTop: 50
  }
});

const Models: React.FC = () => {
  const dispatch = useAppDispatch()
  useEffect(() => {
    dispatch(fetchModels())
  }, [])
  const { models } = useAppSelector(store => store.models)
  const classes = useStyles();

  return <>
    <TableContainer component={Paper} className={classes.container}>
      <Table className={classes.table} size="small" aria-label="a dense table">
        <TableHead>
          <TableRow>
            <TableCell>Model name</TableCell>
            <TableCell>Description</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {models.map((item) => (
            <TableRow key={item.id}>
              <TableCell>
                <Link to={`/models/${item.id}`}>{item.name}</Link>
              </TableCell>
              <TableCell>{item.description}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  </>
}

export default Models