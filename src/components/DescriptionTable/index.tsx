import { useParams } from 'react-router-dom'
import { useAppSelector } from '../../hooks'
import { AttributeInterface } from '../../types/model'
import { makeStyles } from '@material-ui/core/styles'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'
import S from './styles.module.css'

const useStyles = makeStyles({
  container: {
    maxWidth: 600,
    margin: 'auto',
    marginTop: 50
  }
});

const DescriptionTable: React.FC = () => {
  const { id, tableId } = useParams<{id: string, tableId: string}>()
  const stores = useAppSelector(state => state.model.model?.stores)
  let table
  if(stores) {
    const store = stores.find(item => item.id === +id)
    table = store?.tables.find(item => item.id === +tableId)
  }
  const classes = useStyles();
  
  return (
    <div className={S.tableDescription}>
      <h3>Table</h3>
      <table>
        <tbody>
          <tr>
            <td>name</td>
            <td>{table?.name}</td>
          </tr>
          <tr>
            <td>description</td>
            <td>{table?.description}</td>
          </tr>
          <tr>
            <td>transfer</td>
            <td>{table?.isTransfer?.toString()}</td>
          </tr>
          <tr>
            <td>truncate</td>
            <td>{table?.isTruncate?.toString()}</td>
          </tr>
          <tr>
            <td>selectionCriteria</td>
            <td>{table?.selectionCriteria}</td>
          </tr>
        </tbody>
      </table>
      <TableContainer component={Paper} className={classes.container}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell align='center'>Name</TableCell>
              <TableCell align='center'>Description</TableCell>
              <TableCell align='center'>TypePersonalInformation</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {table?.attributes.map((item: AttributeInterface, idx: number) => <TableRow key={idx}>
              <TableCell align='center'>{item.name}</TableCell>
              <TableCell align='center'>{item.description}</TableCell>
              <TableCell align='center'>{item.typePersonalInformation}</TableCell>
            </TableRow>)}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  )
}

export default DescriptionTable