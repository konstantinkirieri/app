import { useEffect } from 'react'
import { fetchModel } from '../../store/slices/model'
import { useAppSelector, useAppDispatch } from '../../hooks'
import { Link, useParams } from 'react-router-dom'
import { toggleFilter } from '../../store/slices/model'
import Toolbar from '@material-ui/core/Toolbar'
import Button from '@material-ui/core/Button'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles({
  toolBar: {
    height: '10vh',
    display: 'flex',
    alignItems: 'center',
    backgroundColor: '#6C8CD5'
  },
  button: {
    marginRight: 7,
    color: 'white'
  },
  link: {
    color: 'white'
  },
  children: {
    flexGrow: 1,
    display: 'flex',
  }
});

const Model: React.FC = ({children}) => {
  useEffect(() => {
    dispatch(fetchModel({modelId}))
  }, [])

  const { modelId } = useParams<{modelId: string}>()

  const { persData } = useAppSelector(state => state.model)
  const dispatch = useAppDispatch()
  const classes = useStyles();

  return <>
    <Toolbar className={classes.toolBar}>
      <Button variant="contained" color="primary" className={classes.button}>
        <Link to={'/models'} className={classes.link}>
          Models
        </Link>
      </Button>
      <Button variant="contained" color="primary" className={classes.button}>
        <Link to={`/models/${modelId}`} className={classes.link}>
          Stores
        </Link>
      </Button>
      <Button variant="contained" color="primary" className={classes.button}
        onClick={() => dispatch(toggleFilter())}>
        {persData ? <span>ALL</span> : <span>with conf</span>}
      </Button>
    </Toolbar>
    <div className={classes.children}>
      {children}
    </div>
  </>
  
}

export default Model