import AppBar from '@material-ui/core/AppBar'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles({
  appBar: {
    height: '10vh',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  appName: {
    fontSize: 30
  }
});

const Header: React.FC = () => {
  const classes = useStyles();

  return (
    <AppBar position='static' className={classes.appBar}>
      <Typography className={classes.appName}>
        Depersonalization
      </Typography>
    </AppBar>
  )
}

export default  Header